import HttpHeaders from './http-headers';
const fetch = window.fetch.bind(window);
export class HttpResponseError {
	constructor( response ) {
		this.response = response;
	}
	get status() {
		return this.response.status;
	}
	get statusText() {
		return this.response.statusText;
	}
	json() {
		return this.response.json();
	}
}
export default class HttpService {
	constructor( fetchBackend = fetch ) {
		this.fetch = fetchBackend;
		this.headers = new HttpHeaders();
	}
	__options( method, body = undefined ) {
		let options = {
			method: method,
			headers: this.headers.toDictionary()
		};
		if ( body != undefined ) {
			if ( this.headers.contentType == 'application/json' )
				options.body = JSON.stringify( body );
			else
				options.body = body;
		}
		return options;
	}
	__request( method, url, body = undefined ) {
		return this
			.fetch( url, this.__options( method, body ) )
			.then( response => {
				if ( !response.ok )
					throw new HttpResponseError( response );
				return response;
			} );
	}
	get( url ) {
		return this.__request( 'GET', url );
	}
	post( url, body = undefined ) {
		return this.__request( 'POST', url, body );
	}
	patch( url, body = undefined ) {
		return this.__request( 'PATCH', url, body );
	}
	put( url, body = undefined ) {
		return this.__request( 'PUT', url, body );
	}
	getJson( url ) {
		return this.__request( 'GET', url ).then( data => data.json() );
	}
	postJson( url, body = undefined ) {
		return this.__request( 'POST', url, body ).then( data => data.json() );
	}
	patchJson( url, body = undefined ) {
		return this.__request( 'PATCH', url, body ).then( data => data.json() );
	}
	putJson( url, body = undefined ) {
		return this.__request( 'PUT', url, body ).then( data => data.json() );
	}
}