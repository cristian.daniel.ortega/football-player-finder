import { types } from '../actions/types';
import createReducer from './creator';

const initialState = {
	players: [],
	isGettingPlayers: false,
	error: false
}

export const playersReducer = createReducer(initialState, {
	[types.SET_GETTING_PLAYERS] (state, action) {
		return { ...initialState, isGettingPlayers: true }
	},
	[types.SET_GETTING_PLAYERS_ERROR] (state, action) {
		return { ...initialState, error: true }
	},
	[types.SET_GETTING_PLAYERS_SUCCESS] (state, action) {
		return { ...initialState, players: action.payload }
	}
})