import { combineReducers } from 'redux';
import * as PlayersReducer from './players';

export default combineReducers({
	...PlayersReducer
});