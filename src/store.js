import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers/index.js';
import HttpService from './services/http';
import thunk from 'redux-thunk';

const api = new HttpService;

export const store = createStore(
    reducers,
    applyMiddleware(thunk.withExtraArgument(api))
);