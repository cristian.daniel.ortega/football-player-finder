import React, { Component } from 'react';
import {
	BrowserRouter as Router,
	Route,
	Link
} from 'react-router-dom'
import Home from './views/home';
import styles from './style/stylesheet.css';

class App extends Component {
	render() {
		return (
			<Router>
				<div>
					<Route exact path="/" component={Home}/>
				</div>
			</Router>
		);
	}
}

export default App;
