let server = 'https://football-players-b31f2.firebaseio.com/';
export default {
    get server() {
      if ( server.endsWith( '/' ) )
        return server;
      return `${server}/`;
    },
    get tokenUrl() {
      return `${server}/oauth/token`;
    },
    get logoutUrl() {
      return `${server}/oauth/revoke`;
    }
}