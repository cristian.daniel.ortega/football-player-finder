String.prototype.isUpperCase = function() { return this.toUpperCase() == this; };
String.prototype.constantize = function() {
	var result = '';
	for ( let i = 0; i < this.length; i++ ) {
		if ( this[ i ].isUpperCase() )
			result += '_';
		result += this[ i ].toUpperCase();
	}
	return result;
};

function createActionCreator( objectToExport, name ) {
	let constant = name.constantize();

	// Action creator
	objectToExport[ name ] = function(arg) {
		return { ...arg, type: constant };
	};
}

export function createActions() {
	let objectToExport = {};
	Array.prototype.forEach.call( arguments, name => createActionCreator(objectToExport, name) );
	return objectToExport;
}