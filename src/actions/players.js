import * as types from './types';
import { createActions } from './creator';
import config from '../config';

export const playersActions = createActions(
	'setGettingPlayers',
	'setGettingPlayersSuccess',
	'setGettingPlayersError'
)

export function getPlayers(){
	return(dispatch, getState, api) => {
		dispatch(playersActions.setGettingPlayers());
		let url = `${config.server}players.json`;
		api.getJson(url)
			.then((response) => {
				dispatch(playersActions.setGettingPlayersSuccess({payload: response}));
				console.log(response);
			})
			.catch((error) => {
				dispatch(playersActions.setGettingPlayersError({payload: error}));
				console.log(error);
			})
	}
}