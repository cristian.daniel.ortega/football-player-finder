function createTypes() {
	let obj = {};
	Array.prototype.forEach.call( arguments, arg => obj[ arg ] = arg );
	return obj;
}

export const types = createTypes(
	'SET_GETTING_PLAYERS',
	'SET_GETTING_PLAYERS_SUCCESS',
	'SET_GETTING_PLAYERS_ERROR'
)