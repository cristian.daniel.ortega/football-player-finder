import React, { Component } from 'react';
import { connectScreen } from '../lib/connect-screen';

class Home extends Component {

	componentDidMount(){
		this.props.getPlayers();
	}

	render(){
		return(
			<div className="generalContainer">
				<h1 className="title">Home</h1>
				<h1 className="title">{this.props.players.length}</h1>
			</div>
		)
	}
}

export default connectScreen( (state) => {
	return {
		players: state.playersReducer.players
	}
}) (Home);