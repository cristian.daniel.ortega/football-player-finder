import { connect } from 'react-redux'
import { ActionsCreator } from '../actions'
import { bindActionCreators } from 'redux'

function mapDispatchToProps(dispatch) {
	return bindActionCreators(ActionsCreator, dispatch);
}

export function connectScreen (mapStateToProps) {
	return connect(mapStateToProps, mapDispatchToProps)
}